package Homework;

import java.util.Scanner;

public class StudentGrade1 {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		System.out.println("Enter your grade: ");
		char grade = myScanner.nextLine().charAt(0);

		if (grade == 'A' || grade == 'B') {
			System.out.println("Perfect! You are so clever!");
		} else if (grade == 'C') {
			System.out.println("Good! But You can do better!");
		} else if (grade == 'D' || grade == 'E') {
			System.out.println("It is not good! You should study!");
		} else if (grade == 'F') {
			System.out.println("Fail! You need to repeat the exam!");
		} else {
			System.out.println("Incorrect grade ");
		}
	}
}







