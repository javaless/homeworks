package Homework;

import java.util.Scanner;

public class MultipleCalculation1 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter the first number:");
		double number1 = Double.parseDouble(myScanner.nextLine());

		System.out.println("Enter operator +, -, *, /, %, p, b or s");
		char operator = myScanner.nextLine().charAt(0);

		System.out.println("Enter the second number:");
		double number2 = myScanner.nextDouble();

		double result;
		boolean result1 = true;

		switch (operator) {
		case '+':
			result = number1 + number2;
			System.out.println("Sum is " + result);
			break;
		case '-':
			result = number1 - number2;
			System.out.println("Quotient is " + result);
			break;
		case '*':
			result = number1 * number2;
			System.out.println("Multiplication result is " + result);
			break;
		case '/':
			result = number1 / number2;
			System.out.println("Devision result is " + result);
			break;
		case '%':
			result = number1 % number2;
			System.out.println("Remainder is " + result);
			break;
		case 'p':
			System.out.println(number1 + " and " + number2);
			break;
		case 'b':
			if (number1 > number2)
				System.out.println(number1 + " is bigger than " + number2);
			else if (number2 > number1)
				System.out.println(number2 + " is bigger than " + number1);
			else
				System.out.println(number1 + " is equal " + number2);
			break;
		case 's':
			if (number1 < number2)
				System.out.println(number1 + " is smaller than " + number2);
			else if (number2 < number1)
				System.out.println(number2 + " is smallerr than " + number1);
			else
				System.out.println(number1 + " is equal " + number2);
			break;
		default:
			System.out.println("You've entered " + operator + ", which is incorrect");
		}

	}
}
