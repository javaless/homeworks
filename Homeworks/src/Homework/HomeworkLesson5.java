package Homework;

import java.util.Scanner;

public class HomeworkLesson5 {

	public static void main(String[] args) {
						
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Enter amount in USD :");
	    double currencyUSD = Double.parseDouble(myScanner.nextLine());
	    double EUR = currencyUSD*0.80;
	    System.out.println("Amount in EUR : "+ EUR);
		
	    System.out.println("Enter amount in EUR :");
	    double currencyEUR = Double.parseDouble(myScanner.nextLine());
	    double USD = currencyEUR/0.80;
	    System.out.println("Amount in USD : "+ USD);
		
		System.out.println("Enter temperature in Fahrenheit :");
        float tempFar = Float.parseFloat(myScanner.nextLine());
        float celsius = ((tempFar-32)*5)/9;
        System.out.println("Temperature in Celsius : "+ celsius);
        
        System.out.println("Enter temperature in Celsius:");
        float tempCel = Float.parseFloat(myScanner.nextLine());
        float fahr = tempCel*9/5 + 32;
        System.out.println("Temperature in Fahrenheit : "+ fahr);
	
        System.out.println("Enter temperature in Celsius :");
        double tempCel1 = Double.parseDouble(myScanner.nextLine());
        double kelvin = tempCel1+273.15;
        System.out.println("Temperature in Kelvin : "+ kelvin);
        
        System.out.println("Enter temperature in Kelvin:");
        double tempKel = Float.parseFloat(myScanner.nextLine());
        double celsius1 = tempKel-273.15;
        System.out.println("Temperature in Celsius : "+ celsius1);
        
		System.out.println("Enter meteres :");
        double meteres = Double.parseDouble(myScanner.nextLine());
        double feet = meteres*3.281;
        System.out.println("Feet : "+ feet);
        
        System.out.println("Enter feet:");
        double feet1 = Double.parseDouble(myScanner.nextLine());
        double metres = feet1/3.281;
        System.out.println("Meters : "+ metres);
        
        System.out.println("Enter square km :");
        int sqKm = Integer.parseInt(myScanner.nextLine());
        double sqMeters = sqKm / 0.000001;
        System.out.println("Square metres : "+ sqMeters);
        
        System.out.println("Enter square meteres:");
        int sqMeteres1 = Integer.parseInt(myScanner.nextLine());
        double sqKm1 = sqMeteres1 * 0.000001;
        System.out.println("Square km : "+ sqKm1);
        
        System.out.println("Enter cubic meteres :");
        double cubMeters = Double.parseDouble(myScanner.nextLine());
        double liters = cubMeters * 1000;
        System.out.println("Liters : "+ liters);
        
        System.out.println("Enter liters:");
        double liters1 = Double.parseDouble(myScanner.nextLine());
        double cubMet = liters1 / 1000;
        System.out.println("Cubic Meteres : "+ cubMet);
	
        System.out.println("Enter kg :");
        double kg = Double.parseDouble(myScanner.nextLine());
        double pounds = kg * 2.20462;
        System.out.println("Pounds : "+ pounds);
        
        System.out.println("Enter pounds:");
        double pounds1 = Double.parseDouble(myScanner.nextLine());
        double kg1 = pounds1 / 2.20462;
        System.out.println("KG : "+ kg1);
        
        System.out.println("Enter number of days :");
        int days = Integer.parseInt(myScanner.nextLine());
        int hours = days * 24;
        System.out.println("Hours : "+ hours);
        
        System.out.println("Enter hourss:");
        int hours1 = Integer.parseInt(myScanner.nextLine());
        int days1 = (hours1/24);
        int hoursRes = hours1%24;
        System.out.println("Days: "+ days1 + " and "+ " Hours residue : " + hoursRes);
	
	}

}
