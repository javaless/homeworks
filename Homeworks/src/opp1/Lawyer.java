package opp1;

import opp.Persona;

public class Lawyer extends Persona {

	private int lawyerID, helpedInCrimesSolving;

	public Lawyer() {

	}

	public Lawyer(String name, String surname, int lawyerID, int helpedInCrimesSolving) {
		super(name, surname);
		this.helpedInCrimesSolving = helpedInCrimesSolving;
		this.lawyerID = lawyerID;
	}


	public int getLawyerID() {
		return lawyerID;
	}

	public void setLawyerID(int lawyerID) {
		this.lawyerID = lawyerID;
	}

	public int getHelpedInCrimesSolving() {
		return helpedInCrimesSolving;
	}

	public void setHelpedInCrimesSolving(int helpedInCrimesSolving) {
		this.helpedInCrimesSolving = helpedInCrimesSolving;
	}

	{
		super.toString();
	}

	public String toString() {
		return super.toString() + System.lineSeparator() + "Lawyer ID: " + this.lawyerID + System.lineSeparator()
				+ "Helped in crime solving: " + this.helpedInCrimesSolving;
	}
	

}
