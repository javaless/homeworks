package opp1;


public class Persona {

	protected String name, surname;

	public Persona() {

	}

	public Persona(String name, String surname) {
		this.name = name;
		this.surname = surname;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String toString() {
		return "Name: " + this.name + System.lineSeparator() + "Surname: " + this.surname;
	}

}
