package opp1;

import java.util.ArrayList;
import java.util.Iterator;


public class District {
			
	
	protected String title, city;
	protected int districtID;
	private ArrayList<Officer> officersInTheDistrict = new ArrayList<Officer>();
	private ArrayList<Lawyer> lawyersInTheDistrict = new ArrayList<Lawyer>();
	private ArrayList<Persona> personsInTheDistrict = new ArrayList<Persona>();	

	public District() {

	}

	public District(String title, String city, int districtID) {
		this.city = city;
		this.districtID = districtID;
		this.title = title;

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getDistrictID() {
		return districtID;
	}

	public void setDistrictID(int districtID) {
		this.districtID = districtID;
	}

	@Override
	public String toString() {

		return "The title:" + this.title + System.lineSeparator() + "City: " + this.city + System.lineSeparator()
				+ "District ID: " + this.districtID + System.lineSeparator() + "There are "
				+ this.personsInTheDistrict.size() + " persons in the district" + System.lineSeparator();
	}

	public void addNewOfficer(Officer officer) {
		this.officersInTheDistrict.add(officer);
	}

	public void removerOfficer(Officer officer) {
		this.officersInTheDistrict.remove(officer);
	}

	public void addNewLawyer(Lawyer lawyer) {
		this.lawyersInTheDistrict.add(lawyer);
	}

	public void removerOfficer(Lawyer lawyer) {
		this.lawyersInTheDistrict.remove(lawyer);
	}

	public void addNewPersona(Persona persona) {
		this.personsInTheDistrict.add(persona);
	}

	public void removerPersona(Persona persona) {
		this.personsInTheDistrict.remove(persona);
	}

	public ArrayList<Persona> getPersonsInTheDistrict() {
		return personsInTheDistrict;
	}

	public void setPersonsInTheDistrict(ArrayList<Persona> personsInTheDistrict) {
		this.personsInTheDistrict = personsInTheDistrict;
		
		
	}

	private ArrayList<Persona> personas ;

	public void setPersonas(ArrayList<Persona> personas) {
		this.personas = personas;
	}

	public float avgLevelOfSkills() {

		int levelTotal = 0, numberOfOfficers = 0;
		Iterator<Persona> iterator = this.personas.iterator();
		while (iterator.hasNext()) {
			Persona persona = iterator.next();

			if (persona instanceof Officer) {
				Officer officer = (Officer) persona;
				numberOfOfficers++;
				levelTotal += officer.getNumberOfOfficers();
			}

		}
		return (float) levelTotal / (float) numberOfOfficers;
	}
}
