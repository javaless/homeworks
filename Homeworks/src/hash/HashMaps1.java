package hash;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


public class HashMaps1 {

	public static void main(String[] args) {
		Map<Double,String> productsHash = new HashMap<Double,String>();

		productsHash.put(0.75,"apple");
		productsHash.put(2.1,"orange");
		productsHash.put(3.1,"grapes");
		productsHash.put(6.7,"melon");
		productsHash.put(4.1,"watermelon");
		productsHash.put(5.25,"mango");
		productsHash.put(2.51,"pear");
		productsHash.put(1.1,"lemon");
		System.out.println(productsHash+System.lineSeparator());

		for (Map.Entry<Double,String> e : productsHash.entrySet())
			System.out.println("Product : " + e.getValue() + " " + " Price : " + e.getKey() );

		Map<Double,String> productsHash1 = new HashMap<Double,String>();
		productsHash.put(8.75,"pineapple");
		productsHash.put(3.97,"tagerine");
		productsHash.put(5.1,"cherry");

		productsHash.putAll(productsHash1);
		System.out.println(System.lineSeparator()+productsHash + System.lineSeparator());
		
		

	    		
		Map<Double,String> map = new TreeMap<Double,String>(productsHash);
		System.out.println("Sorting price by ascending order: ");
		Set<java.util.Map.Entry<Double, String>> set = map.entrySet();
		Iterator<java.util.Map.Entry<Double, String>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry me = (Map.Entry) iterator.next();
			System.out.print(me.getValue() + ": ");
			System.out.println(me.getKey());
		
	}	
	}	
}
