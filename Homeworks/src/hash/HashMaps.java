package hash;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class HashMaps {

	public static void main(String[] args) {
		Map<Integer, Product> productsHash = new HashMap<Integer, Product>();

		Product product1 = new Product("apple", 0.75);
		Product product2 = new Product("orange", 2.1);
		Product product3 = new Product("grapes", 3.1);
		Product product4 = new Product("pear", 2.51);
		Product product5 = new Product("melon", 6.7);
		Product product6 = new Product("watermelon", 4.1);
		Product product7 = new Product("mango", 4.25);
		Product product8 = new Product("lemon", 1.1);

		productsHash.put(1, product1);
		productsHash.put(2, product2);
		productsHash.put(3, product3);
		productsHash.put(4, product4);
		productsHash.put(5, product5);
		productsHash.put(6, product6);
		productsHash.put(7, product7);
		productsHash.put(8, product8);
		printMap(productsHash);

		Map<Integer, Product> productsHash1 = new HashMap<Integer, Product>();
		Product product9 = new Product("pineapple", 8.75);
		Product product10 = new Product("tagerine", 3.97);
		Product product11 = new Product("cherry", 5.1);

		productsHash.put(9, product9);
		productsHash.put(10, product10);
		productsHash.put(11, product11);

		productsHash.putAll(productsHash1);
		printMap(productsHash);
		
		
	}

	public static void printMap(Map<Integer, Product> collection) {
		Iterator<Product> iterator = collection.values().iterator();
		while (iterator.hasNext()) {
			Product product = iterator.next();
			System.out.println("Product name: " + product.getName() + " - " + "Product price :" + product.getPrice());

		}

		Product minProductPrice = null;
		Iterator<Product> iterator1 = collection.values().iterator();
		while (iterator1.hasNext()) {
			Product currentProductPrice = iterator1.next();
			if (minProductPrice == null || minProductPrice.getPrice() > currentProductPrice.getPrice())
				minProductPrice = currentProductPrice;

		}

		System.out.println(System.lineSeparator()+minProductPrice.getName() + " has the min price");

		Product maxProductPrice = null;
		Iterator<Product> iterator2 = collection.values().iterator();
		while (iterator2.hasNext()) {
			Product currentProductPrice = iterator2.next();
			if (maxProductPrice == null || maxProductPrice.getPrice() < currentProductPrice.getPrice())
				maxProductPrice = currentProductPrice;
		}
		System.out.println(System.lineSeparator()+maxProductPrice.getName() + " has the max price");

		
		
		}    
		}  
		
	
