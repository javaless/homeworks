package opp;

import java.util.ArrayList;
import java.util.Iterator;

public class District {

	protected String title, city;
	protected int districtID;
	private ArrayList<Officer> officersInTheDistrict = new ArrayList<Officer>();

	public District() {

	}

	public District(String title, String city, int districtID) {
		this.city = city;
		this.districtID = districtID;
		this.title = title;

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getDistrictID() {
		return districtID;
	}

	public void setDistrictID(int districtID) {
		this.districtID = districtID;
	}

	@Override
	public String toString() {

		return "The title:" + this.title + System.lineSeparator() + "City: " + this.city + System.lineSeparator()
				+ "District ID: " + this.districtID + System.lineSeparator() + "There are "
				+ this.officersInTheDistrict.size() + " officers in the district";
	}

	public void addNewOfficer(Officer officer) {
		this.officersInTheDistrict.add(officer);
	}

	public void removerOfficer(Officer officer) {
		this.officersInTheDistrict.remove(officer);
	}

	public float calculateAvgLevellInDistrict() {
		float levelSum = 0;
//		for (int i = 0; i < this.officersInTheDistrict.size(); i++)
//		levelSum += this.officersInTheDistrict.get(i).calculateLevel();
//		return levelSum / this.officersInTheDistrict.size();
//	}

		Iterator<Officer> iterator = this.officersInTheDistrict.iterator();
		while (iterator.hasNext())
			levelSum += iterator.next().calculateLevel();
		return levelSum / this.officersInTheDistrict.size();

	}

}
