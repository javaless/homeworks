package opp;

import java.util.ArrayList;
import java.util.Iterator;

public class Execution {

	public Execution() {

	}

	public static void main(String[] args) {

		Officer[] officers = new Officer[7];
		District[] districts = new District[2];
		// Lawyer[] lawyers = new Lawyer[3];

		for (int i = 0; i < 7; i++)
			officers[i] = new Officer();

		for (int i = 0; i < 2; i++)
			districts[i] = new District();

		District district1 = new District("NY", "Brooklyn", 111);
		District district2 = new District("NY", "Bronx", 222);

		for (int i = 0; i < 3; i++)
			district1.addNewOfficer(officers[i]);

		for (int i = 3; i < 7; i++)
			district2.addNewOfficer(officers[i]);

		Lawyer lawyer1 = new Lawyer("Julliette", "March", 54678, 11);
		Lawyer lawyer2 = new Lawyer("Harry", "Cool", 56783, 32);
		Lawyer lawyer3 = new Lawyer("April", "Smart", 143412, 45);

		ArrayList<Lawyer> lawyers = new ArrayList<Lawyer>();
		lawyers.add(lawyer1);
		lawyers.add(lawyer2);
		lawyers.add(lawyer3);

		Lawyer bestLawyer = null;
		Iterator<Lawyer> iterator = lawyers.iterator();
		while (iterator.hasNext()) {
			Lawyer currentLawyer = iterator.next();
			if (bestLawyer == null || bestLawyer.getHelpedInCrimesSolving() < currentLawyer.getHelpedInCrimesSolving())
				bestLawyer = currentLawyer;

		}

		int totalHelps = 0;
		Iterator<Lawyer> iteratorLawyers = lawyers.iterator();
		while (iteratorLawyers.hasNext()) {
			Lawyer lawyer = iteratorLawyers.next();
			totalHelps += lawyer.getHelpedInCrimesSolving();

			System.out.println(district1);
			System.out.print(System.lineSeparator());
			System.out.println(district2);
			System.out.print(System.lineSeparator());

			System.out.println(lawyer1);
			System.out.print(System.lineSeparator());

			System.out.println(lawyer2);
			System.out.print(System.lineSeparator());

			System.out.println(lawyer3);
			System.out.print(System.lineSeparator());

			System.out.println(bestLawyer.getName() + " " + bestLawyer.getSurname() + " did the most helps in crime solving : "+
			+bestLawyer.getHelpedInCrimesSolving() + " helps");
			System.out.print(System.lineSeparator());

			System.out.println("There are " + totalHelps + " total helps in crime solving by lawyers with ");

//		System.out.println("There are "+ (lawyer1.getHelpedInCrimesSolving() + lawyer2.getHelpedInCrimesSolving()
//		+ lawyer3.getHelpedInCrimesSolving())+ " total helps in crime solving by lawyers");
		}
	}
}
