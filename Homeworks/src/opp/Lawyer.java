package opp;

public class Lawyer extends Persona {

	// private String name, surname;
	private int lawyerID, helpedInCrimesSolving;

	public Lawyer() {

	}

	public Lawyer(String name, String surname, int lawyerID, int helpedInCrimesSolving) {
//		this.name = name;
//		this.surname = surname;
		super(name, surname);
		this.helpedInCrimesSolving = helpedInCrimesSolving;
		this.lawyerID = lawyerID;
	}

//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getSurname() {
//		return surname;
//	}
//
//	public void setSurname(String surname) {
//		this.surname = surname;
//	}
//
	public int getLawyerID() {
		return lawyerID;
	}

	public void setLawyerID(int lawyerID) {
		this.lawyerID = lawyerID;
	}

	public int getHelpedInCrimesSolving() {
		return helpedInCrimesSolving;
	}

	public void setHelpedInCrimesSolving(int helpedInCrimesSolving) {
		this.helpedInCrimesSolving = helpedInCrimesSolving;
	}

	{
		super.toString();
	}

	public String toString() {
		return super.toString() + System.lineSeparator() + "Lawyer ID: " + this.lawyerID + System.lineSeparator()
				+ "Helped in crime solving: " + this.helpedInCrimesSolving;
	}

}
