package opp;

public class Officer extends Persona {

	public int officerID, crimesSolved;

	public Officer() {
	}

	public Officer(String name, String surname, int officerID, int crimesSolved) {// // argument constructor
		super(name, surname);
		this.crimesSolved = crimesSolved;
		this.officerID = officerID;
	}

	public int getOfficerID() {
		return officerID;
	}

	public void setOfficerID(int officerID) {
		this.officerID = officerID;
	}

	public int getCrimesSolved() {
		return crimesSolved;
	}

	public void setCrimesSolved(int crimesSolved) {
		this.crimesSolved = crimesSolved;

	}

	@Override
	public String toString() {
		return super.toString() + "Officer ID: " + this.officerID + System.lineSeparator() + "Crimes Solved: "
				+ this.crimesSolved + System.lineSeparator() + "Level: " + this.calculateLevel();
	}

	int calculateLevel() {
		if (this.crimesSolved < 20)
			return 1;
		else if (this.crimesSolved >= 20 && this.crimesSolved < 40)
			return 2;
		else
			return 3;

	}

	public static void main(String[] args) {

		Officer officer1 = new Officer();
		Officer officer2 = new Officer();
		Officer officer3 = new Officer();
		Officer officer4 = new Officer();
		Officer officer5 = new Officer();
		Officer officer6 = new Officer();
		Officer officer7 = new Officer();

		officer1.setCrimesSolved(15);
		officer1.setName("John");
		officer1.setSurname("Clain");
		officer1.setOfficerID(43626);

		officer2.setCrimesSolved(10);
		officer2.setName("Mike");
		officer2.setSurname("Cramer");
		officer2.setOfficerID(1426);

		officer3.setCrimesSolved(34);
		officer3.setName("Jason");
		officer3.setSurname("Kane");
		officer3.setOfficerID(2326);

		officer4.setCrimesSolved(25);
		officer4.setName("Sam");
		officer4.setSurname("Brown");
		officer4.setOfficerID(34262);

		officer5.setCrimesSolved(56);
		officer5.setName("Tom");
		officer5.setSurname("White");
		officer5.setOfficerID(51474);

		officer6.setCrimesSolved(33);
		officer6.setName("Jane");
		officer6.setSurname("Smart");
		officer6.setOfficerID(9876);

		officer6.setCrimesSolved(93);
		officer6.setName("Hanna");
		officer6.setSurname("Hood");
		officer6.setOfficerID(8716);

		officer7.setCrimesSolved(19);
		officer7.setName("Anna");
		officer7.setSurname("Nice");
		officer7.setOfficerID(76543);

		System.out.println("The first officer:");
		System.out.println(officer1);
		System.out.print(System.lineSeparator());

		System.out.println("The second officer:");
		System.out.println(officer2);
		System.out.print(System.lineSeparator());

		System.out.println("The third officer:");
		System.out.println(officer3);
		System.out.print(System.lineSeparator());

		System.out.println("The forth officer:");
		System.out.println(officer4);
		System.out.print(System.lineSeparator());

		System.out.println("The fifth officer:");
		System.out.println(officer5);
		System.out.print(System.lineSeparator());

		System.out.println("The sixth officer:");
		System.out.println(officer6);
		System.out.print(System.lineSeparator());

		System.out.println("The seventh officer:");
		System.out.println(officer7);
		System.out.print(System.lineSeparator());

	}
}
